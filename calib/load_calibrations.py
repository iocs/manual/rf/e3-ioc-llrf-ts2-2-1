import glob
import os
import pathlib

from epics import caput

path = pathlib.Path(__file__).parent.absolute()
print(path)

# clean old clean files
file_rm = glob.glob('*clean*')
for f in file_rm:
    os.remove(f)

file_names = glob.glob('*.csv')

for file_name in file_names:
    file_result_name = file_name[:-4] + "_clean.csv"
    # clean the last comment lines
    with open(file_name, "r") as f:
        lines = f.readlines()
        while lines[-1][0] == "#" or lines[-1][0] == "\n":
            lines = lines[:-1]
        with open(file_result_name, "w") as f_result:
            f_result.writelines(lines)

    pv_name = file_name[:-4].replace("_", ":") + "-CalCSV"
    print(pv_name)
    caput(pv_name, str(path) + "/" + file_result_name)
